#!/bin/bash
set -euo pipefail

[ "$UID" -eq 0 ] || [ ! $(groups "$USER" | grep -q '\bdocker\b') ] || \
  { echo "This script must be run as root."; exit 1; }

BUILD="."

docker stop learn-server || true
docker build -t tdps:learn-server "$BUILD/"
docker container rm learn-server || true

echo 'Use Control-P Control-Q to detach without terminating.'
echo 'RUNS IN PRODUCTION DATABASE'

# Allocate TTY for the convenience of sigkill
docker run -t --env-file k8s/env.list --name learn-server tdps:learn-server