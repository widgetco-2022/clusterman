# Pseudo install script

# Make sure to give your current user cluster admin privilidges
kubectl create clusterrolebinding cluster-admin-binding --clusterrole=cluster-admin \
    --user=$(gcloud config get-value core/account)

kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.3.1/cert-manager.yaml
