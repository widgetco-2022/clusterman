#!/usr/bin/env node
import { KubeConfig, NetworkingV1beta1Api } from '@kubernetes/client-node';
const kc = new KubeConfig()
const myArgs = process.argv.slice(2);

const CLIENT_IDENTIFIER = myArgs[0] // 'tristate'

kc.loadFromDefault()
const k8sApi = kc.makeApiClient(NetworkingV1beta1Api)

function createIngress(cid, subdomain) {
  return {
    metadata: {
      name: `production-custom-${cid}`,
      annotations: {
        'kubernetes.io/ingress.class': 'nginx',
        'nginx.org/server-snippets': 'gzip on;'
      },
    },
    spec: {
      rules: [
        {
          host: `${subdomain}.tdps.app`,
          http: {
            paths: [
              {
                backend: {
                  serviceName: 'production-auto-deploy',
                  servicePort: 5000
                },
                path: '/'
              }
            ]
          }
        }
      ],
      tls: [
        {
          hosts: [`${subdomain}.tdps.app`]
        }
      ]
    },
  }
}

function main (cid) {
  if ( typeof cid !== 'string' || cid.length < 3) {
    console.log('Please pass a subdomain for the client, ex tristate')
    return 1
  }
  
  const adminSubDomain = cid + '-admin'
  const adminIngress = createIngress(cid, adminSubDomain)
  const studentIngress = createIngress(cid, cid)
  // console.log(k8s.dumpYaml(ingress))
  k8sApi.createNamespacedIngress('learn-admin', adminIngress)
    .then(() => console.log(`Admin Ingress ${adminSubDomain} success.`))
    .catch(e => {
      console.log(e.response.body.details)
      console.log(e)
    })
  k8sApi.createNamespacedIngress('learn-web', studentIngress)
    .then(() => console.log(`Student Ingress ${cid} success.`))
    .catch(e => {
      console.log(e.response.body.details)
      console.log(e)
    })
}

main(CLIENT_IDENTIFIER)
