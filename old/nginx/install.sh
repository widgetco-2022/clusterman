# pseudo install script
# https://docs.nginx.com/nginx-ingress-controller/installation/installation-with-manifests/

$ git clone https://github.com/nginxinc/kubernetes-ingress/
$ cd kubernetes-ingress/deployments
$ git checkout v1.11.2
$ git clone https://github.com/nginxinc/kubernetes-ingress/
$ cd kubernetes-ingress/deployments
$ git checkout v1.11.2
kubectl apply -f rbac/rbac.yaml

# Must be cluster admin to do this:
$ kubectl apply -f rbac/ap-rbac.yaml
$ kubectl apply -f common/default-server-secret.yaml
$ kubectl apply -f common/nginx-config.yaml

# 1.18 needs this, customize it first
$ kubectl apply -f common/ingress-class.yaml

$ kubectl apply -f common/crds/k8s.nginx.org_virtualservers.yaml
$ kubectl apply -f common/crds/k8s.nginx.org_virtualserverroutes.yaml
$ kubectl apply -f common/crds/k8s.nginx.org_transportservers.yaml
$ kubectl apply -f common/crds/k8s.nginx.org_policies.yaml

$ kubectl apply -f common/crds/k8s.nginx.org_globalconfigurations.yaml

$ kubectl apply -f common/global-configuration.yaml

# DeamonSet installation
$ kubectl apply -f daemon-set/nginx-ingress.yaml
